import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'api.dart';

class ErrorDisplayWidget extends StatelessWidget {
  ErrorDisplayWidget(this.error, {Key key}) : super(key: key);

  final Object error;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.error, color: Colors.redAccent[400], size: 28),
        Text('Error: $error', style: TextStyle(fontSize: 16.0))
      ],
    );
  }
}

class GameModeImage extends StatelessWidget {
  GameModeImage(this.gameMode, this.width, this.isLarge, {Key key}) : super(key: key);

  static const String _gameModeURL = "https://overwatcharcade.today/api/gamemodes";

  final GameMode gameMode;
  final double width;
  final bool isLarge;

  static Future<Map<int, Map<bool, String>>> _gameModeImages = _getGameModesImages();
  static Map<int, Map<bool, Future<String>>> _imageFutureInstances = Map();

  static Future<Map<int, Map<bool, String>>> _getGameModesImages({bool download = false}) async {
    File file;
    if (download) {
      FileInfo fileInfo = await DefaultCacheManager().downloadFile(_gameModeURL);
      file = fileInfo.file;
    } else {
      file = await DefaultCacheManager().getSingleFile(_gameModeURL);
    }

    return new Map.fromIterable(
      jsonDecode(await file.readAsString()),
      key: (jsonMap) => jsonMap["id"],
      value: (jsonMap) {
        return new Map<bool, String>.fromIterable(
          jsonMap["image"].entries,
          key: (jsonMap) => jsonMap.key == "large",
          value: (jsonMap) => jsonMap.value as String,
        );
      },
    );
  }

  static Future<String> _getImageURL(GameMode gameMode, bool large) async {
    if (!_imageFutureInstances.containsKey(gameMode.id) || !_imageFutureInstances[gameMode.id].containsKey(large)) {
      // if gameMode is not known yet, update gameModes
      if (!(await _gameModeImages).containsKey(gameMode.id)) {
        _gameModeImages = _getGameModesImages(download: true);
      }

      _imageFutureInstances.putIfAbsent(gameMode.id, () => Map<bool, Future<String>>());

      _imageFutureInstances[gameMode.id][large] = _gameModeImages.then((value) => value[gameMode.id][large]);
    }

    return _imageFutureInstances[gameMode.id][large];
  }

  final double largeAspectRatio = 455 / 648;
  final double normalAspectRatio = 260 / 648;

  @override
  Widget build(BuildContext context) {
    final double aspectRatio = this.isLarge ? largeAspectRatio : normalAspectRatio;

    final Widget placeholder = SizedBox(
      height: aspectRatio * this.width,
      width: this.width,
    );

    return FutureBuilder(
      future: _getImageURL(gameMode, this.isLarge),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState != ConnectionState.done && !snapshot.hasData) {
          return placeholder;
        } else if (snapshot.hasError) {
          return ErrorDisplayWidget(snapshot.error);
        } else {
          return CachedNetworkImage(
            imageUrl: snapshot.data,
            placeholder: (context, string) => placeholder,
            errorWidget: (context, url, error) => new Icon(Icons.error),
            width: this.width,
          );
        }
      },
    );
  }
}

class GameModeCard extends StatelessWidget {
  GameModeCard(this.gameMode, this.width, this.isLarge, {Key key}) : super(key: key);

  final GameMode gameMode;
  final bool isLarge;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color(0xffe7e7eb),
      shape: BeveledRectangleBorder(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GameModeImage(gameMode, width, this.isLarge),
          Container(
            width: width,
            padding: EdgeInsets.all(4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  gameMode.players.toUpperCase(),
                  softWrap: false,
                  style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Color(0xff1c75bc),
                  ),
                ),
                Text(
                  gameMode.name.toUpperCase(),
                  softWrap: false,
                  style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Color(0xff28354f),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
