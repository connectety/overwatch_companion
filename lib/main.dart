import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;

import 'api.dart';
import 'widgets.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Overwatch Companion',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
        scaffoldBackgroundColor: Colors.grey[900],

        textTheme: Typography().white,
        dividerColor: Colors.white,
      ),
      home: MainScreen(),
    ),
  );
}

class MainScreen extends StatelessWidget {
  MainScreen() : this.background = _getRandomBackground();

  final AssetImage background;

  static AssetImage _getRandomBackground() {
    List<String> backgrounds = [
      "BlizzardWorld.jpg",
      "Busan.jpg",
      "ChateauGuillard.jpg",
      "EcopointAntarctica.jpg",
      "Eichenwalde.jpg",
      "Hanamura.jpg",
      "Hollywood.jpg",
      "HorizonLunarColony2.jpg",
      "Junkertown.jpg",
      "KingsRow.jpg",
      "Oasis.jpg",
      "Paris.jpg",
      "Rialto.jpg",
      "Route66.jpg",
      "TempleOfAnubis.jpg",
      "VolskayaIndustries.jpg",
      "WatchpointGibraltar.jpg"
    ];

    Random random = new Random();
    String randomFileName = backgrounds[random.nextInt(backgrounds.length)];
    return new AssetImage(path.join("assets/backgrounds/", randomFileName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: background,
              fit: BoxFit.cover,
            ),
          ),
          child: GameModesPage(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            FadeRoute(SettingsScreen()),
          );
        },
        child: Icon(Icons.settings),
      ),
    );
  }
}

class GameModesPage extends StatefulWidget {
  GameModesPage({Key key}) : super(key: key);

  @override
  GameModesPageState createState() {
    return new GameModesPageState();
  }
}

class GameModesPageState extends State<GameModesPage> {
  Future<Map<String, GameMode>> _futureGameModes = getToday();

  Future<void> _handleRefresh() {
    setState(() {
      _futureGameModes = getToday(download: true);
    });

    return _futureGameModes;
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _handleRefresh,
      child: LayoutBuilder(
        builder: (context, constraints) {
          return SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: ConstrainedBox(
              constraints: BoxConstraints(minWidth: constraints.maxWidth, minHeight: constraints.maxHeight),
              child: SafeArea(
                child: Center(
                  child: FutureBuilder<Map<String, GameMode>>(
                    future: _futureGameModes,
                    builder: (BuildContext context, AsyncSnapshot<Map<String, GameMode>> snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return CircularProgressIndicator();
                      } else if (snapshot.hasError) {
                        return ErrorDisplayWidget(snapshot.error);
                      } else {
                        return GameModeGrid(snapshot.data);
                      }
                    },
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Container(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.4),
                ),
              ),
            ),
          ),
          Material(
            color: Colors.transparent,
            child: ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.5, color: Colors.white),
                    ),
                    color: Colors.grey.withOpacity(0.4),
                  ),
                  child: Text(
                    "Settings",
                    style: TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                ListTile(
                  title: Text("Notification settings"),
                  onTap: () {},
                ),
                Divider(height: 0),
                ListTile(
                  title: Text("Invalidate Image Cache"),
                  trailing: FlatButton(
                    child: Icon(
                      Icons.refresh,
                      color: Colors.white,
                    ),
                    color: Colors.grey.withOpacity(0.45),
                    onPressed: () {},
                  ),
                ),
                Divider(height: 0),
                ListTile(
                  title: Text("Used Librarys"),
                  onTap: () => showLicensePage(context: context),
                ),
                Divider(height: 0),
                ListTile(
                  title: Text("About"),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pop(context),
        child: Icon(Icons.close),
      ),
    );
  }
}

class GameModeGrid extends StatelessWidget {
  GameModeGrid(this.gameModes, {Key key}) : super(key: key);

  final Map<String, GameMode> gameModes;

  @override
  Widget build(BuildContext context) {
    double cardWidth = 180;

    List<Widget> children = <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GameModeCard(gameModes["tile_1"], cardWidth, true),
          Column(
            children: <Widget>[
              GameModeCard(gameModes["tile_2"], cardWidth, false),
              GameModeCard(gameModes["tile_5"], cardWidth, false),
            ],
          ),
        ]
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              GameModeCard(gameModes["tile_3"], cardWidth, false),
              GameModeCard(gameModes["tile_6"], cardWidth, false),
            ],
          ),
          Column(
            children: [
              GameModeCard(gameModes["tile_4"], cardWidth, false),
              GameModeCard(gameModes["tile_7"], cardWidth, false),
            ],
          ),
        ],
      ),
    ];

    if (MediaQuery.of(context).orientation == Orientation.landscape) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: children,
      );
    } else {
      return Column(
        children: children,
      );
    }
  }
}

class FadeRoute extends PageRouteBuilder {
  final Widget page;

  FadeRoute(this.page) : super(
    opaque: false,
    pageBuilder: (
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
    ) {
      return page;
    },
    transitionsBuilder: (
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child,
    ) {
      return FadeTransition(opacity: animation, child: child);
    },
  );
}
