import 'dart:convert';
import 'dart:io';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';

Future<Map<String, GameMode>> getToday({bool download = false}) async {
  final DateTime now = DateTime.now().toUtc();

  final DateTime lastMidnight = new DateTime.utc(now.year, now.month, now.day);
  List<dynamic> json = await _getLatestJSON("https://overwatcharcade.today/api/today", lastMidnight, download);

  if (json.length < 1) {
    int dayDifference = now.weekday - DateTime.tuesday;

    if (dayDifference.isNegative) {
      dayDifference += 7;
    }

    final DateTime lastTuesdayMidnight = new DateTime.utc(now.year, now.month, now.day - dayDifference);
    json = await _getLatestJSON("https://overwatcharcade.today/api/week", lastTuesdayMidnight, download);

     // todo: warn that content is outdated
  }

  Map<dynamic, dynamic> gamemodes = json.first;
  return new Map.fromIterable(
    ["tile_1", "tile_2", "tile_3", "tile_4", "tile_5", "tile_6", "tile_7"],
    key: (tileName) => tileName,
    value: (tileName) => GameMode.fromJson(gamemodes[tileName]),
  );
}

Future<dynamic> _getLatestJSON(String url, DateTime lastValidTime, bool download) async {
  if (download) {
    return await _downloadJSON(url);
  }

  File file = await DefaultCacheManager().getSingleFile(url);
  // todo: handle null if not cached and no connection
  dynamic json = jsonDecode(await file.readAsString());

  // if are not any gameModes set for today yet, the server returns an empty JSON array (normally its a JSON map)
  // always update if there is no gameMode set yet
  if (json is List) {
    return await _downloadJSON(url);
  }

  final DateTime updateTime = DateTime.parse(json["updated_at"]).toUtc();
  if (updateTime.isBefore(lastValidTime)) {
    return await _downloadJSON(url);
  }

  return json;
}

Future<dynamic> _downloadJSON(String url) async {
  FileInfo fileInfo = await DefaultCacheManager().downloadFile(url);

  String content = await fileInfo.file.readAsString();

  return jsonDecode(content);
}

class GameMode {
  GameMode._(this.id, this.name, this.players);

  final int id;
  final String name;
  final String players;

  GameMode.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"],
        players = json.containsKey("players") ? json["players"] : "6v6";

  @override
  String toString() {
    return "${this.name} (${this.players})";
  }
}
